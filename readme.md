## Crear un repositoio en GitLab

### by Óscar Ousinde Suárez

- ¿Algo que considero que me hace diferente?

  I don't really know if this makes me different but I feel confortable thinking that I'm a person who never gives up and that loves to learn new things. I have the confidence, perseverance and patience to do it. This is one of the reasons why programming is so exiting to me. Every day I like to think that I'm a little bit better developer than yesterday, and a little worse than tomorrow. That's what encourage me day after day and that's a good way to make a difference. That just makes me feel good! Wich is a very important thing for my self-fulfilment.

- ¿Qué es para mí Git?

  Git for me is the tool to have total control over my code and the code of the team I work with. At first it can be tricky but then you realize It's the best way to organize and take care of your code. Also, makes easy to work with people and organize daily work.

- ¿Qué es para mí Docker?

  Docker is a way to pack you applications with all the dependencies and libraries they need to work correctly. Your application will be stored into a container that runs on top of the host operating system. This is a big difference between Docker containers and virtual machines that virtualize all the operating system, making them slower and heavier than Docker containers.

- ¿Qué es para mí el Testing?

  Testing is a very important field in software development. For me it ensures the code to behave equals as developers think it should behave. That's why is critical to make good testing of our applications before deploy them. Sometimes, testing can be seen as a waste of time, in terms that it takes a lot of time. However, testing time is good time spent.

- ¿Qué características de JavaScript conozco ahora mismo?

  - Basics on JS (methods, loops, variables...).
  - Asynchrony.
  - Promises.
  - Async/await.
  - Fetch.
  - V8: I think is very important to know how JS works internally and basic concepts like Event Loop, Stack and Callback Queue.

- ¿Qué son para mí los Web Components?

  Web Components are for me packs of different technologies used to build reusable custom elements. They are particulary interesting, as they are based on basic technologies like HTML, CSS and JS. I see them as a look back to the basics on the web with an incredible solid future in software development.

- Cualquier otra cosa que quieras compartir que consideres que te hace única

  As I said before, my confidence, patience and perseverance. Also, I love to cook and my lasagna is the best in the world! At least, the best I ever taste 😋.
